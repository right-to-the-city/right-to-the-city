import yaml
import json

from dataclasses import dataclass

# TODO заменить на нормальный ReConfig
class BaseConfig:
    """Config that can read itself from files."""

    @classmethod
    def from_dict(cls, d: dict):
        """Create a config object from a dictionary."""

        args = {}
        for attr, type_ in cls.__annotations__.items():
            # Работаем только с примитивными типами, пока не критично
            if attr in d and isinstance(type_, type) \
                    and issubclass(type_, BaseConfig):
                args[attr] = type_.from_dict(d[attr])
            else:
                if (value := d.get(attr)) is not None:
                    args[attr] = value

        return cls(**args)  # type: ignore

    @classmethod
    def from_yaml(cls, path: str):
        """Read config from a YAML/YML file."""

        with open(path, 'r', encoding='utf-8') as f:
            raw_config = yaml.safe_load(f)

        return cls.from_dict(raw_config)

    @classmethod
    def from_json(cls, path):
        """Read config from a JSON file."""

        with open(path, 'r', encoding='utf-8') as f:
            raw_config = json.load(f)

        return cls.from_dict(raw_config)


@dataclass
class Config(BaseConfig):
    db_path: str
    api_root: str
