# Right To The City Backend

Бэкенд/API для проекта Право на город.

## Установка

Установить MySQL ([Пример инструкции по устанвке](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04))

Создать виртуальное окружение по желанию

Поставить зависимости:

```sh
pip install -r requirements.txt
```

Создать файл config.yml со следующим содержимым:

```yml
db_path: <Строка подключения к MySQL> # в формате `mysql+mysqlconnector://{username}:{password}@{host}:{port}`
```


## Запуск

Dev-сервер:

```sh
python -m server -p <port> -d
```

Production-сервер - запустить `server:app` при помощи любого WSGI/ASGI сервера. Например gunicorn:

```sh
gunicorn server:app
```
