from datetime import datetime

from flask_login import UserMixin

from . import app, db


class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=True,
    )

    user_type = db.Column(db.String(10))

    surveys = db.relationship('Survey', back_populates='user')

    __mapper_args__ = {
        'polymorphic_on': 'user_type',
        'polymorphic_identity': 'base',
    }


class WebUser(User, UserMixin):
    __tablename__ = 'web_user'

    id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)

    username = db.Column(db.String(32), unique=True)
    password = db.Column(db.String(32))
    name = db.Column(db.String(45))
    priority = db.Column(db.Integer)
    email = db.Column(db.String(45), unique=True)

    __mapper_args__ = {
        'polymorphic_identity': 'web',
    }

    def __init__(self, username, password, name, priority, email):
        self.username = username
        self.password = password
        self.name = name
        self.priority = priority
        self.email = email


class TelegramUser(User):
    __tablename__ = 'tg_user'

    id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)

    telegram_id = db.Column(db.BigInteger, unique=True, nullable=False)
    telegram_username = db.Column(db.String(32), nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': 'tg',
    }

    @property
    def username(self):
        return self.telegram_username

    @property
    def name(self):
        return self.telegram_username


class Survey(db.Model):
    __tablename__ = 'survey'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    lat = db.Column(db.Double)
    lon = db.Column(db.Double)
    category = db.Column(db.String(45))
    rating = db.Column(db.Integer)
    comment = db.Column(db.Text)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    user = db.relationship('User', back_populates='surveys')
    address = db.relationship('Address', back_populates='survey', uselist=False)
    
    media = db.relationship('Media', back_populates='survey')

    tags = db.relationship('Tag', back_populates='survey')

    def __init__(self, lat, lon, category, rating, comment, date, user_id):
        self.lat = lat
        self.lon = lon
        self.category = category
        self.rating = rating
        self.comment = comment
        self.date = date
        self.user_id = user_id


class Media(db.Model):
    __tablename__ = 'media'

    id = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=True,
        nullable=False,
    )

    link = db.Column(db.String(45))
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))
    survey = db.relationship('Survey', back_populates='media')

    def __init__(self, link, survey_id):
        self.link = link
        self.survey_id = survey_id

    @property
    def url(self):
        return f'/uploads/{self.link}'
    
    @property
    def filename(self):
        return str(app.config['UPLOAD_FOLDER'] / self.link)


class Tag(db.Model):
    __tablename__ = 'tags'

    id = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=True,
        nullable=False,
    )
    text = db.Column(db.String(45))
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))

    survey = db.relationship('Survey', back_populates='tags')

    def __init__(self, text, survey_id):
        self.text = text
        self.survey_id = survey_id


class Rating(db.Model):
    __tablename__ = 'rating'

    id = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=True,
        nullable=False,
    )
    value = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))

    def __init__(self, value, user_id, survey_id):
        self.value = value
        self.user_id = user_id
        self.survey_id = survey_id


class Comment(db.Model):
    __tablename__ = 'comment'

    id = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=True,
        nullable=False,
    )
    text = db.Column(db.Text)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))

    def __init__(self, text, date, user_id, survey_id):
        self.text = text
        self.date = date
        self.user_id = user_id
        self.survey_id = survey_id


class Address(db.Model):
    __tablename__ = 'address'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'), unique=True, nullable=False)

    name = db.Column(db.String(255))      # Название поселения
    type = db.Column(db.String(255))      # Тип поселения

    country = db.Column(db.String(255))   # Страна
    region = db.Column(db.String(255))    # Регион
    state = db.Column(db.String(255))     # Область
    county = db.Column(db.String(255))    # Район
    district = db.Column(db.String(255))  # Район города

    survey = db.relationship('Survey', back_populates='address', single_parent=True)

    def __init__(self, name='', type='', country='', region='', state='', county='', district='') -> None:
        super().__init__()

        self.name = name
        self.type = type
        self.country = country
        self.region = region
        self.state = state
        self.county = county
        self.district = district
