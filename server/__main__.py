import sys
from argparse import ArgumentParser

from . import app, tasks

argv = sys.argv[1:]

if 'run' not in argv and 'task' not in argv:
    # Запустить сервер, если не указана команда
    argv = ['run'] + argv


parser = ArgumentParser(
    prog='server',
    description='Start the server',
)

sub = parser.add_subparsers(help='Commands', dest='command')

run = sub.add_parser('run', help='Run the server (can be omited)')

run.add_argument(
    '-p', '--port', type=int, default=80,
    help='Specify port to listen on'
)
run.add_argument(
    '-d', '--debug', action='store_true',
    help='Run server in debug mode'
)

task = sub.add_parser('task', help='Perform one of the tasks')
tasks.setup(task)

args = parser.parse_args(argv)

if args.command == 'run':
    app.run(host='0.0.0.0', port=args.port, debug=args.debug)

elif args.command == 'task':
    with app.app_context():
        tasks.do_task(args.task, args)