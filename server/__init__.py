import logging
import os
import sys
from datetime import datetime
from pathlib import Path

from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

from sqlalchemy_utils import database_exists, create_database

from common.config import Config

log_file = Path('logs') / datetime.now().strftime('%Y_%m_%d_%H_%M_%S.log')

logging.basicConfig(
    level=logging.INFO,

    handlers=[
        logging.StreamHandler(sys.stdout),
        logging.FileHandler(log_file)
    ],
)

app = Flask(__name__)

config_path = 'config.yml'
config = Config.from_yaml(config_path)
app.logger.info(f'Reading config from {config_path}')

# Настройка авторизации
login_manager = LoginManager(app)
login_manager.login_view = 'login'


DB_NAME = os.environ.get('DB_NAME', 'city_prod')

# TODO сделать по-нормальному :) Как я вообще додумался до такого?
*_, uploads_subfolder = DB_NAME.split('_')

# Настройка папок
UPLOAD_FOLDER = Path(f'uploads/{uploads_subfolder}').resolve()
UPLOAD_FOLDER.mkdir(exist_ok=True)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.logger.info(f'Set {UPLOAD_FOLDER = }')

# Настройки БД
database_uri = f'{config.db_path}/{DB_NAME}'
app.logger.info(f'Set {database_uri = }')

app.config['SQLALCHEMY_DATABASE_URI'] = database_uri
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = 'your_secret_key_here'

db = SQLAlchemy(app)

# Это должно быть здесь, так как models использует db
from .models import WebUser

# Подготовка БД (если требуется)
with app.app_context():
    if not database_exists(database_uri):
        create_database(database_uri)

    db.create_all()

    admin = WebUser.query.filter_by(priority=1).first()

    if admin is None:
        # Создать админов, если их нет в бд
        admin = WebUser(
            'admin', 'admin', 'Administrator', 1, 'admin@example.org'
        )

        db.session.add(admin)
        db.session.commit()

        app.logger.info('No admins found. Created default')

from . import api_routes
