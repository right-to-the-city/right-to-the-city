from collections import Counter
from transformers import pipeline
import re

from sqlalchemy.sql.expression import func
from flask_sqlalchemy.query import Query

from .analysis_module import AnalysisModule
from ..models import Survey

def load_words_from_txt(txt_file):
    """
    Загружает список слов из текстового файла.

    Args:
        txt_file (str): Путь к текстовому файлу.

    Returns:
        List[str]: Список слов.
    """
    with open(txt_file, 'r', encoding='utf-8') as file:
        words = {word.strip() for word in file.readlines()}
    return words


def analyze_word_frequency(texts):
    """
    Анализирует частоту встречаемости слов в текстах.

    Args:
        texts (List[str]): Список текстов.

    Returns:
        Counter: Объект Counter, содержащий частоту встречаемости слов.
    """
    all_words = []
    for text in texts:
        all_words.extend(re.findall(r'\b\w+\b', text.lower()))
    word_counts = Counter(all_words)
    return word_counts


profanity_words = load_words_from_txt('text/swearwords.txt')
stopwords = load_words_from_txt('text/stopwords.txt')


def has_profanity(text):
    """
    Проверяет, содержит ли текст матерные слова.

    Args:
        text (str): Текст для проверки.

    Returns:
        bool: True, если текст содержит матерные слова, иначе False.
    """
    tokenized = re.findall(r'\b\w+\b', text.lower())
    return bool(set(tokenized) & profanity_words)


# Загрузка модели для анализа тональности
sentiment_analyzer = pipeline("sentiment-analysis", model="blanchefort/rubert-base-cased-sentiment")


class SentimentModule(AnalysisModule):
    def extract(self, query: Query[Survey]) -> dict:
        """
        Извлекает комментарии из запроса.

        Args:
            query (Query[Survey]): Запрос к базе данных.

        Returns:
            dict: Словарь с комментариями.
        """
        return {
            'comments': [
                survey.comment for survey in query
            ]
        }

    def process(self, data: dict) -> dict:
        """
        Анализирует данные о комментариях.

        Args:
            data (dict): Словарь с данными о комментариях.

        Returns:
            dict: Словарь с результатами анализа.
        """
        profanity_comment_count = 0
        total_comment_count = 0
        total_neutral_score = 0
        total_positive_score = 0
        total_negative_score = 0
        all_words = []
        neutral_comments = []
        positive_comments = []
        negative_comments = []
        mixed_comments = []

        sentiment_column = []
        profanity_column = []
        for text in data['comments']:
            total_comment_count += 1

            # Анализ тональности с помощью модели Hugging Face
            result = sentiment_analyzer([text])[0]
            label = result['label']
            score = result['score']

            if label == 'neutral':
                total_neutral_score += score
                neutral_comments.append(text)
            elif label == 'positive':
                total_positive_score += score
                positive_comments.append(text)
            elif label == 'negative':
                total_negative_score += score
                negative_comments.append(text)
            else:
                mixed_comments.append(text)

            sentiment_column.append(label)

            # Проверка на матерные слова
            if has_profanity(text):
                profanity_comment_count += 1
                profanity_column.append(True)
            else:
                profanity_column.append(False)

            # Анализ частоты слов
            word_frequency = analyze_word_frequency([text])
            all_words.extend(word_frequency.keys())

        # Фильтрация слов (удаление матерных, стоп-слов и цифр)
        all_words = [
            word for word in all_words 
            if word.lower() not in profanity_words 
            and word.lower() not in stopwords 
            and not word.isdigit()
        ]
        top_10_words = Counter(all_words).most_common(10)

        # Расчет процентов тональности
        total_score = total_neutral_score + total_positive_score + total_negative_score
        
        # Check if total_score is zero to avoid division by zero
        if total_score == 0:
            neutral_percent = 0
            positive_percent = 0
            negative_percent = 0
        else:
            neutral_percent = (total_neutral_score / total_score) * 100
            positive_percent = (total_positive_score / total_score) * 100
            negative_percent = (total_negative_score / total_score) * 100

        return {
            "profanity_comment_count": profanity_comment_count,
            "total_comment_count": total_comment_count,
            "neutral_percent": neutral_percent,
            "neutral_comments": neutral_comments,
            "positive_percent": positive_percent,
            "positive_comments": positive_comments,
            "negative_percent": negative_percent,
            "negative_comments": negative_comments,
            "mixed_comments": mixed_comments,
            "top_10_words": top_10_words,

            'columns': {
                'sentiment': sentiment_column,
                'profanity': profanity_column,
            }
        }

    def display(self, data: dict) -> None:
        """
        Отображает результаты анализа.

        Args:
            data (dict): Словарь с результатами анализа.
        """
        print("Процент нейтральных комментариев:", data["neutral_percent"])
        print("Процент положительных комментариев:", data["positive_percent"])
        print("Процент отрицательных комментариев:", data["negative_percent"])

        print("Самые часто используемые слова (без матерных):")
        for word, count in data["top_10_words"]:
            print(f"{word.title()}: {count}")

        print("\n----------------------")
        print("Всего комментариев:", data["total_comment_count"])
        print("Количество комментариев с матерными словами:", data["profanity_comment_count"])


def main():
    """
    Основная функция для тестирования модуля.
    """
    query = Survey.query.filter(
        Survey.address.has(name='Волгоград') & (func.length(Survey.comment) >= 10)
    )

    sent = SentimentModule()
    sent.extract_process_display(query)


if __name__ == "__main__":
    main()