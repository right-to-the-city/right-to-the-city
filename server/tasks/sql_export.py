import csv

from ..models import Survey


def task(output = 'output.csv'):
    surveys: list[Survey] = Survey.query.filter(
        Survey.date.between('2023-10-01', '2024-01-31')
    ).all()

    data = [
        [
            survey.id,
            survey.lat,
            survey.lon,
            survey.category,
            survey.rating,
            survey.date,
            survey.user_id,
            survey.user.user_type,
            survey.address.country,
            survey.address.name,
            survey.address.district,
        ]
        for survey in surveys
    ]

    headers = ['id', 'lat', 'lon', 'category', 'rating', 'date', 'user_id', 'platform', 'country', 'city', 'district']
    data.insert(0, headers)



    with open(output, 'w', encoding='utf-8') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerows(data)


