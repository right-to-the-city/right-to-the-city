from abc import ABC, abstractmethod

from flask_sqlalchemy.query import Query

from ..models import Survey


class AnalysisModule(ABC):
    @abstractmethod
    def extract(self, query: Query[Survey]) -> dict:
        """Extract relevant data from queryset"""
    
    @abstractmethod
    def process(self, data: dict) -> dict:
        """Perform analysis"""
    
    @abstractmethod
    def display(self, data: dict) -> None:
        """Display the results of analysis"""
    
    def extract_process(self, query: Query[Survey]) -> tuple[dict, dict]:
        idata = self.extract(query)
        odata = self.process(idata)

        return idata, odata
    
    def process_display(self, data: dict) -> tuple[dict, dict]:
        odata = self.process(data)
        self.display(odata)

        return data, odata

    def extract_process_display(self, query: Query[Survey]) -> tuple[dict, dict]:
        idata, odata = self.extract_process(query)

        self.display(odata)

        return idata, odata
    
    