from sklearn.cluster import KMeans # библиотека для анализа и обучения

# библиотеки для вывода
import numpy as np
import matplotlib.pyplot as plt

from flask_sqlalchemy.query import Query
from sqlalchemy.sql.expression import func

from .analysis_module import AnalysisModule
from ..models import Survey, Media

class ClasterizationModule(AnalysisModule):
    def __init__(self, n_clusters=3):
        self.n_clusters = n_clusters

    def extract(self, query: Query[Survey]) -> dict:
        """
        Возвращает словарь вида:
        {
            'survey': [
                {
                    'latitude': 'value',    # координата широты
                    'longitude': 'value',   # координата долготы
                    'category': 'value',    # категория
                    'rating': 'value',      # рейтинг записи
                    'city': 'value',        # Город
                },
            ]
        }
        """

        return {
            'survey': [
                {
                    'latitude': survey.lat,
                    'longitude': survey.lon,
                    'category': survey.category,
                    'rating': survey.rating,
                    'city': getattr(survey.address, 'name', None)
                }
                for survey in query
            ]
        }


    def process(self, data: dict) -> dict:
        """
        Возвращает словарь с данными для кластеризации:
        {
            'geodata': [(longitude, latitude), ...],  # список кортежей с координатами (долгота, широта)
            'categories': ['category', ...],  # список категорий
            'ratings': ['rating', ...],  # список рейтингов
            'cities': ['city', ...],  # список городов
            'unique_cities': set()  # уникальные города
        }
        """

        geodata = [(row['longitude'], row['latitude']) for row in data['survey']]

        lon, lat = zip(*geodata)
        min_lat = min(lat)
        max_lat = max(lat)
        min_lon = min(lon)
        max_lon = max(lon)

        # не менее трех записей в городе нужно для кластеризаци, но я немного увеличила,
        # т.к. в некоторых городах ровно три записи, это захламляет и вывод информации и график
        if len(geodata) < max(20, self.n_clusters):
            raise ValueError(f'Unable to clasterize - not enough points: {len(geodata)} < {max(20, self.n_clusters)}')

        kmeans = KMeans(n_clusters=self.n_clusters)
        kmeans.fit(geodata)

        result = {
            'geodata': geodata,
            'cluster': kmeans.labels_,
            'cluster_centers': kmeans.cluster_centers_,
            'lat_range': (min_lat, max_lat),
            'lon_range': (min_lon, max_lon),
        }

        result['columns'] = {
            'cluster': result['cluster']
        }

        return result


    def display(self, data: dict) -> None:
        geodata = data['geodata']
        cluster = data['cluster']
        centers = data['cluster_centers']

        min_lat, max_lat = data['lat_range']
        min_lon, max_lon = data['lon_range']

        kmeans = KMeans(n_clusters=self.n_clusters, init=centers)
        kmeans.fit(centers)

        xx, yy = np.meshgrid(np.linspace(min_lon, max_lon, 100), np.linspace(min_lat, max_lat, 100))
        Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)

        fig, ax = plt.subplots()

        # world = gpd.read_file(gpd.datasets.get_path('naturalearth_cities')) # Читаем границы городов
        # world.plot(ax=ax, color='lightgrey') # Отображаем границы городов

        # Установим пределы карты в соответствии с минимальными и максимальными значениями широты и долготы
        ax.set_xlim(min_lon, max_lon)
        ax.set_ylim(min_lat, max_lat)

        ax.contourf(xx, yy, Z, alpha=0.3, cmap='rainbow')
        ax.scatter(*zip(*geodata), c=cluster, cmap='rainbow')
        ax.scatter(centers[:, 0], centers[:, 1], marker='x', color='black')

        # Настройка отображения
        ax.set_xlabel('Longitude')
        ax.set_ylabel('Latitude')
        ax.set_title('City Clusters')

        plt.savefig('clasterization_results.png')


def main():
    query = Survey.query.filter(
        Survey.address.has(name='Волгоград')
    )

    # query = Survey.query.filter(
    #     Survey.address.has(name='Волгоград')   # Только волгоград
    #     & (func.length(Survey.comment) >= 10)  # Осмысленные комментарии
    #     & Survey.media.any(                    # Подходящие форматы фото
    #         Media.link.like('%.png') | 
    #         Media.link.like('%.jpg') |
    #         Media.link.like('%.jpeg') |
    #         Media.link.like('%.webp')
    #     )
    #     & Survey.date.between('2023-10-01', '2024-01-31')  # Период тестирования
    # ) 

    clast = ClasterizationModule(n_clusters=3)
    _, odata = clast.extract_process_display(query)

    # print(odata['cluster_centers'])

if __name__ == '__main__':
    main()
