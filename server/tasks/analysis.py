import pandas as pd

from sqlalchemy.sql.expression import func

from ..models import Survey, Media

from .clasterization import ClasterizationModule
from .images import RecognitionModule
from .sentiment import SentimentModule


def transpose(input_list: list[dict]) -> dict[list]:
    if not input_list:
        return {}
    
    result = {}

    for key in input_list[0]:
        result[key] = [dict_.get(key) for dict_ in input_list]

    return result


def main():
    query = Survey.query.filter(
        (func.length(Survey.comment) >= 10)  # Осмысленные комментарии
        & Survey.media.any(                    # Подходящие форматы фото
            Media.link.like('%.png') | 
            Media.link.like('%.jpg') |
            Media.link.like('%.jpeg') |
            Media.link.like('%.webp')
        )
    )
    clast = ClasterizationModule(30)
    recog = RecognitionModule()
    sent = SentimentModule()

    data = transpose([
        {
            'id': survey.id,
            'lat': survey.lat,
            'lon': survey.lon,
            'category': survey.category,
            'rating': survey.rating,
            'city': survey.address.name,
            'district': survey.address.district, 
        } for survey in query
    ])

    for module in (clast, recog, sent):
        _, odata = module.extract_process(query)
        data |= odata['columns']

    df = pd.DataFrame(data)

    print(df)
    df.to_csv('results.csv', index=False)


def add_address():
    df = pd.read_csv('results.csv', index_col=False)
    df = df.set_index('id')

    query = Survey.query.filter(
        Survey.address.has(name='Волгоград')   # Только волгоград
        & (func.length(Survey.comment) >= 10)  # Осмысленные комментарии
        & Survey.media.any(                    # Подходящие форматы фото
            Media.link.like('%.png') | 
            Media.link.like('%.jpg') |
            Media.link.like('%.jpeg') |
            Media.link.like('%.webp')
        )
        # & Survey.date.between('2023-10-01', '2024-01-31')  # Период тестирования
    )

    data = transpose([
        {
            'id': survey.id,
            'city': survey.address.name,
            'district': survey.address.district, 
        } for survey in query
    ])

    rdf = pd.DataFrame(data)
    rdf = rdf.set_index('id')

    df = df.join(rdf, how='left').reset_index()

    df.to_csv('results2.csv', index=False)