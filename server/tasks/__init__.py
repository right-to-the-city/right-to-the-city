from argparse import ArgumentParser

from . import geocode, sql_export, sentiment, clasterization, images, analysis

meta = {
    'geocode': {
        'command': geocode.task,
        'help': 'Reverse geocode all submissions that don\'t have address set.',
    },
    'sql_export': {
        'command': sql_export.task,
        'help': 'Perform sql export to csv file.',
        'arguments': {
            'output': {
                'flags': ['--output', '-o'],
                'kwargs': {
                    'help': 'Output file name',
                    'default': 'output.csv',
                },
            },
        },
    },
    'sentiment': {
        'command': sentiment.main,
        'help': 'Get sentiment of comments',
    },
    'clasterization': {
        'command': clasterization.main,
        'help': 'Clasterization of geodata',
    },
    'images': {
        'command': images.main,
        'help': 'Image recognition',
    },
    'analysis': {
        'command': analysis.main,
        'help': 'Combined data analysis',
    },
}


def setup(parser: ArgumentParser):
    commands = parser.add_subparsers(help='Available tasks', dest='task')

    for name, info in meta.items():
        parser = commands.add_parser(name, help=info.get('help', 'No help provided'))

        for argument in info.get('arguments', {}).values():
            parser.add_argument(*argument['flags'], **argument['kwargs'])


def do_task(name, args):
    kwargs = {
        arg: getattr(args, arg)
        for arg in meta[name].get('arguments', ())
    }

    meta[name]['command'](**kwargs)
