from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter

from .. import app, db
from ..models import Address, Survey


def task():
    app.logger.info('Starting geocoding')

    # TODO Тут возможно контакты указать (и брать их из конфига)
    nominatim = Nominatim(user_agent='right-to-the-city')
    # Просят ограничить частоту запросов, делаем паузу в 5 секунд между ними
    reverse = RateLimiter(nominatim.reverse, min_delay_seconds=1)

    added = 0
    skipped = 0

    try:
        surveys: list[Survey] = Survey.query.all()
        
        for survey in surveys:
            # Не трогаем уже существующий адрес
            if survey.address is not None:
                skipped += 1
                continue

            data = reverse(f'{survey.lat}, {survey.lon}')
            if data:
                survey.address = make_address(data.raw['address'])
                db.session.commit()
                added += 1
                app.logger.info(f'Добавлено адресов: {added} из {len(surveys)}')
            else:
                app.logger.error(f'No address for {survey.id}')
    except:
        app.logger.exception('Stopping geocoding due to exception:')
    else:
        app.logger.info('Geocoding finished successfully')
    finally:
        app.logger.info(f'{added} rows added, {skipped} rows skipped, {added + skipped} total')


def make_address(data):
    settlement_types = ('city', 'town', 'village', 'hamlet', 'locality', 'municipality', 'tourism', 'quarter')
    type_ = next((t for t in settlement_types if t in data), 'unknown')

    return Address(
        data.get(type_),
        type_,
        data.get('country'),
        data.get('region'),
        data.get('state'),
        data.get('county'),
        data.get('city_district')
        or data.get('district')
        or data.get('neighbourhood'),
    )


def get_address_for(survey: Survey):
    try:
        nominatim = Nominatim(user_agent='right-to-the-city')
        data = nominatim.reverse(f'{survey.lat}, {survey.lon}')

        if data:
            return make_address(data.raw['address'])
        else:
            app.logger.error(f'No address for {survey.id}')
    except:
        app.logger.exception(f'Unable to get address for {survey.id} due to an exception:')
    
    return None
