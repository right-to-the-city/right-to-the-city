from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input, decode_predictions
import numpy as np

from flask_sqlalchemy.query import Query

from .analysis_module import AnalysisModule
from ..models import Survey,Media


def get_data(filename=None, *, limit=100):
    if filename is None:
        media = Media.query.filter(Media.link.like('%.png')).limit(limit)
    else:
        media = Media.query.filter(Media.link.like('%' + filename))

    return {
        'images': [
            medium.filename for medium in media
        ]
    }

general_categories = {
    'здания и сооружения': [
        'palace', 'castle', 'church', 'prison', 'monastery', 'greenhouse', 'stone_wall', 'mosque', 'water_tower', 'barn', 'dock', 'cinema', 'sliding_door', 'altar', 'aircraft_carrier', 
        'rain_barrel', 'grille',  'geyser', 'mountain_tent', 'carousel', 'stage', 'solar_dish', 'grocery_store', 'airliner', 'space_shuttle', 'barbershop', 'tobacco_shop', 
        'bakery', 'cliff_dwelling', 'container_ship', 'shoe_shop', 'studio_couch', 'drilling_platform',  'cradle', 'chiffonier', 'crib', 'planetarium',
    ],

    'транспорт': [
        'streetcar', 'passenger_car', 'electric_locomotive', 'steam_locomotive', 'freight_car', 'trolleybus', 'minivan', 'school_bus', 'cab', 'ambulance', 'police_van', 
        'bullet_train', 'horse_cart', 'tricycle', 'moving_van', 'bicycle-built-for-two', 'minibus', 'Model_T', 'mountain_bike', 'fire_engine', 'limousine', 'jeep', 
        'moped', 'jinrikisha', 'recreational_vehicle',  'mobile_home', 'barrow', 'garbage_truck', 'tractor', 'snowplow', 'golfcart', 'airship',  
        'gas_pump', 'go-kart', 'pickup', 'liner', 'tank',  'forlkit', 'snowmobile', 'schooner', 'warplane',  'convertible', 'racer',
        'spaghetti_squash', 'carbonara', 'ice_cream', 'French_loaf', 'kite', 'can_opener', 'plane',
    ],

    'еда': [
        'guacamole', 'butternut_squash', 'ice_cream', 'bagel', 'pizza', 'beer_bottle', 'lollipop', 'head_cabbage', 'Granny_Smith', 'pot', 'menu', 'ice_lolly', 'pretzel', 'shopping_basket',  
        'pomegranate', 'strawberry', 'red_wine', 'acorn', 'pineapple', 'zucchini', 'honeycomb', 'lemon', 'cucumber', 'banana', 'hotdog', 'bell_pepper', 'cauliflower', 'custard_apple', 'broccoli', 
        'artichoke', 'rotisserie', 'frying_pan', 'dishwasher', 'carbonara', 'French_loaf', 'ice_cream', 'bagel', 'butternut_squash', 'pizza', 'eggnog', 'burrito', 'mashed_potato', 'potpie', 
        'chocolate_sauce', 'dough', 'consomme', 'pineapple', 'corn', 
    ],

    'ограждения': [
        'chainlink_fence', 'worm_fence', 'picket_fence', 'bannister',  'doormat', 'carton', 'rocking_chair', 'chain_mail', 'fence',
    ],

    'уличный интерьер и мебель': [
        'park_bench', 'bell_cote', 'fountain',  'folding_chair', 'mailbag', 'fire_screen', 'theater_curtain', 'shower_curtain', 'mailslot', 'washbasin', 'space_heater',
        'four-poster', 'dining_table', 'desk', 'china_cabinet', 'bassinet', "potter's_wheel",
    ],

    'памятники': [
        'triumphal_arch', 'obelisk', 'hourglass', 'monument', 'sand', 'coral_fungus', 'vase',
    ],

    'природные объекты': [
        'lakeside', 'valley', 'alp', 'pinwheel', 'stupa', 'megalith', 'bubble', 'promontory', 'spider_web', 'geyser', 'marmot', 'slug', 'black_swan', 'fireboat', 'crane', 'sandbar', 
        'acorn', 'daisy', 'lacewing', 'tiger_beetle', 'mushroom', 'brain_coral',  'agaric', 'slot','water_ouzel', 'coral_reef', 'gar', 'bolete', 'flatworm', 'mosquito_net', 'dowitcher',
        'albatross', 'sea_cucumber', 'jacamar', 'great_white_shark', 'otter', 'three-toed_sloth', 'chiton', 'stingray', 'electric_ray', 'leatherback_turtle', 'puffer', 'banded_gecko', 
        'anemone_fish', 'platypus', 'frilled_lizard', 'rock_beauty', 'dugong', 'chambered_nautilus', 'tiger_shark', 'jellyfish',
    ],

    'городское окружение': [
        'traffic_light', 'breakwater', 'street_sign', 'pier', 'parking_meter', 'manhole_cover', 'mailbox', 'garbage_truck', 'traffic_light', 'shop', 'boathouse', 'pot', 
        'street_sign', 'scale', 'padlock', 'tent', 'parking_meter', 'bakery', 'mailbox', 'power_drill', 'soap_dispenser', 'butcher_shop', 'trashcan',
    ],
                            
    'техника и инструменты': [
        'brass', 'cannon', 'crane', 'scoreboard', 'spotlight', 'safety_pin', 'lumbermill', 'punching_bag', 'missile', 'hook', 'wallaby', 'vending_machine', 'vacuum', 'projectile', 
        'projuctile', 'shovel', 'bottlecap', 'coil', 'plow', 'swab', 'syringe', 'broom', 'file', 'gong', 'ice_lolly', 'combination_lock', 'screen', 'apiary', 'racket', 
        'hair_spray', 'hatchet', 'harmonica',  'reel', 'chain_saw', 'accordion', 'screw', 'trombone', 'computer_keyboard', 'cassette', 'flute', 'paddle', 'banjo', 'reflex_camera', 'toaster', 
        'hammer', 'cassette_player', 'tape_player', 'radio', 'slide_rule', 'drake', 'ballpoint', 'screwdriver', 'sewing_machine', 'drum', 'modem', 'oscilloscope', 
        'maraca', 'remote_control', 'photocopier', 'projector', 'pencil_sharpener', 'table_lamp', 'space_heater', 'rotisserie', 'frying_pan', 'photocopier', 'remote_control', 'projector', 
        'CD_player', 'spatula', 'pencil_sharpener', 'medicine_chest', 'waffle_iron', 'table_lamp', 'microwave', 'measuring_cup', 'wooden_spoon', 'hand_blower', 'home_theater', 'Petri_dish', 
        'oil_filter', 'espresso_maker', 'mixing_bowl', 'letter_opener', 'hair_slide', 'drum', 'washer'
    ],

    'одежда': [
        'bulletproof_vest', 'thimble', 'wool', 'cloak', 'trench_coat', 'fur_coat', 'kimono', 'peacock', 'miniskirt', 'sweatshirt', 'abaya', 'poncho',
        'military_uniform', 'prayer_rug', 'cardigan', 'gown', 'suit', 'cowboy_hat', 'stole', 'cardigan', 'cowboy_boot', 'feather_boa', 'hip', 'vestment', 'sarong', 'bow_tie', 'running_shoe', 'kuvasz', 
        'refrigerator', 'Loafer', 'swimming_trunks', 'ski_mask', 'neck_brace', 'bonnet', 'bulletproof_vest', 'bikini', 'bath_towel', 'sombrero', 'Cardigan', 'overskirt', 'maillot', 
        'wool', 'velvet', 'jersey', 'brassiere', 'sandal', 'apron', 'bib', 'necklace', 'pajama', 'lipstick', 'mitten', 'handkerchief', 'shower_cap', 'chiffonier', 'pajama', 'dishrag', 'lipstick', 
        'mitten', 'handkerchief', 'diaper', 'shower_cap', 'hamper'
    ],

    'животные': [
        'llama', 'bull', 'gazelle', 'wire-haired_fox_terrier', 'West_Highland_white_terrier', 'llama', 'meerkat', 'Scotch_terrier', 'Appenzeller', 'bison', 'Irish_terrier', 
        'groenendael', 'Irish_wolfhound', 'silky_terrier', 'soft-coated_wheaten_terrier', 'giant_schnauzer', 'Scottish_deerhound', 'Lakeland_terrier', 'standard_poodle', 'schipperke',
        'Siberian_husky', 'macaque', 'bighorn', 'stretcher', 'Eskimo_dog', 'Samoyed', 'collie', 'Great_Pyrenees', 'ram', 'cairn', 'magpie', 'half_track', 'Airedale', 'Border_collie', 
        'Bedlington_terrier', 'Norwich_terrier', 'Doberman', 'basenji', 'komondor', 'Slug', 'Newfoundland', 'baboon', 'EntleBucher', 'bluetick', 'ballplayer', 'Bernese_mountain_dog',
        'toy_terrier', 'wild_boar', 'coyote', 'skunk', 'quail', 'Ibizan_hound', 'centipede', 'Pug', 'American_black_bear', 'Shetland_sheepdog', 'Border_terrier', 'Bouvier_des_Flandres', 
        'cheetah', 'armadillo', 'robin', 'whippet', 'Kerry_blue_terrier', 'malinois', 'basset', 'Weimaraner', 'Staffordshire_bullterrier', 'Blenheim_spaniel', 'Indian_elephant', 'Chihuahua', 
        'Greater_Swiss_Mountain_dog', 'vulture', 'Arabian_camel', 'timber_wolf', 'jaguar', 'langur', 'black-and-tan_coonhound', 'golden_retriever', 'ibex', 'Lhasa', 'toy_poodle', 'borzoi', 
        'rhinoceros_beetle', 'spider_monkey', 'Mexican_hairless', 'hen', 'standard_schnauzer', 'Rhodesian_ridgeback', 'Ostrich', 'Afghan_hound', 'Sealyham_terrier', 'Old_English_sheepdog', 
        'Pomeranian', 'dingo', 'great_grey_owl', 'kelpie', 'Leonberg', 'Siamang', 'tusker', 'English_foxhound', 'Australian_terrier', 'Welsh_springer_spaniel', 'tiger_cat', 'king_penguin', 
        'boxer', 'partridge', 'tiger', 'Siamese_cat', 'white_wolf', 'Egyptian_cat', 'Brittany_spaniel', 'redbone', 'Maltese_dog', 'African_hunting_dog', 'American_Staffordshire_terrier', 
        'orangutan', 'bull_mastiff', 'Shih-Tzu', 'grey_fox', 'bloodhound', 'woirabbit', 'flamingoi', 'Angora', 'Madagascar_cat', 'wombat', 'squirrel_monkey', 'English_springer', 'gazelle', 
        'Tibetan_terrier', 'gorilla', 'sloth_bear', 'goldfinch', 'eft', 'papillon', 'boa_constrictor', 'Saint_Bernard', 'otterhound', 'Arctic_fox', 'killer_whale', 'giant_panda', 
        'howler_monkey', 'Chesapeake_Bay_retriever', 'dhole', 'beagle', 'kit_fox', 'wolf_spider', 'black_stork', 'polecat', 'curly-coated_retriever', 'cougar', 'grasshopper', 'hamster', 
        'sea_lion', 'macaw', 'common_iguana', 'chimpanzee', 'guenon', 'flat-coated_retriever', 'garden_spider', 'weasel', 'house_finch', 'tarantula', 'titi', 'limpkin', 'mongoose', 'patas',
        'Dandie_Dinmont', 'impala', 'colobus', 'proboscis_monkey', 'pelican', 'Yorkshire_terrier', 'black_widow', 'German_short-haired_pointer', 'box_turtle', 'bittern', 'lycaenid', 
        'lesser_panda', 'indri', 'mouse', 'ruddy_turnstone', 'American_coot', 'badger', 'little_blue_heron', 'black_and_gold_garden_spider', 'African_grey', 'cockroach', 'junco', 'capuchin',
        'damselfly', 'ptarmigan', 'Sussex_spaniel', 'Gila_monster', 'chickadee', 'agama', 'hornbill', 'cricket', 'hippopotamus', 'Japanese_spaniel', 'mud_turtle', 'European_gallinule',
        'Brabancon_griffon', 'black-footed_ferret', 'mantis', 'sulphur_butterfly', 'ringlet', 'leafhopper', 'spiny_lobster', 'Komodo_dragon', 'bulbul', 'prairie_chicken', 'axolotl', 'mouse', 
        'koala', 'sidewinder', 'sea_snake', 'waffle_iron', 'rock_python', 'bullfrog', 'junco', 'water_ouzel', 'coral_reef', 'spotted_salamander', 'bulbul', 'prairie_chicken', 
        'ruddy_turnstone', 'American_coot', 'badger', 'little_blue_heron', 'African_grey', 'cockroach', 'sturgeon', 'ice_bear', 'green_mamba', 'capuchin', 'Sussex_spaniel', 'Gila_monster', 
        'ptarmigan', 'Indian_cobra', 'hippopotamus', 'Japanese_spaniel', 'mud_turtle', 'European_gallinule', 'black-footed_ferret', 'cabbage_butterfly', 'mantis', 'sulphur_butterfly',
        'Brabancon_griffon', 'gyromitra', 'Dungeness_crab', 'ringlet', 'leafhopper', 'spiny_lobster', 'Komodo_dragon', 'koala', 'sidewinder', 'sea_snake', 'mink', 'lionfish', 'scorpion', 
        'African_crocodile', 'king_crab', 'tench', 'grey_whale', 'albatross', 'jacamar', 'great_white_shark', 'otter', 'three-toed_sloth', 'tiger_shark', 'platypus', 'frilled_lizard',
        'rock_beauty', 'dugong', 'chambered_nautilus', 'jellyfish', 'earthstar', 'tree_frog', 'starfish', 'rock_python', 'bullfrog', 'spotted_salamander',  'goldfish', 'crayfish', 'isopod', 'tailed_frog', 'American_lobster', 
        'African_chameleon', 'red-backed_sandpiper', 'sea_anemone', 'rock_crab', 'sea_slug', 'grey_whale', 'great_white_shark', 'otter', 'bald_eagle', 
        'three-toed_sloth', 'chiton', 'stingray', 'electric_ray', 'leatherback_turtle', 'puffer', 'banded_gecko', 'anemone_fish', 'platypus', 'frilled_lizard', 'rock_beauty', 'dugong',
        'chambered_nautilus', 'tiger_shark', 'jellyfish', 'Italian_greyhound', 'coho', 'oystercatcher', 'sturgeon', 'sea_snake', 'Dungeness_crab', 'ostrich', 'snail', 'hummingbird', 'miniature_poodle', 
        'chow', 'drake', 'flamingo', 'fly', 'American_alligator', 'African_hunting_dog', 'fox_squirrel', 'warthog', 'bee', 'green_snake', 'pelican', 'ringneck_snake', 'ice_bear', 'axolotl', 
    ],

    'водный транспорт': [
        'fireboat', 'container_ship', 'speedboat', 'catamaran', 'trimaran', 'canoe', 'gondola', 'canoe', 
        'lifeboat', 'paddlewheel', 'speedboat', 'trimaran', 'catamaran',
    ] ,

    'ямы, лужы и прочее': [
        'ashcan', 'hole', 'caldron', 'wreck', 'puddle', 'drumstick', 'slot'
    ],

    'спорт': [
        'horizontal_bar', 'golfcart', 'punching_bag', 'racer', 'bobsled', 'tank', 'go-kart', 'pick', 'hook', 'basketball', 'ski', 'parallel_bars', 'soccer_ball', 'rugby_ball', 
        'croquet_ball', 'tennis_ball', 'football_helmet', 'baseball', 'golf_ball', 'balance_beam', 'French_horn', 'steel_drum', 'puck', 'volleyball', 'pool_table', 'cricket', 'maraca', 'cricket',
        'bicycle-built-for-two',
    ],

    'мебель': [
        'upright', 'folding_chair', 'grille', 'rocking_chair', 'bathtub', 'fountain_pen', 'lampshade', 'shower_curtain', 'throne', 'toyshop', 'wardrobe', 'cradle', 'bookcase', 'chiffonier',
        'crib', 'pillow', 'scabbard', 'pillow', 'Dutch_oven', 'crib', 'cup',
    ],

    'принадлежности': [
        'flagpole', 'mailbox', 'padlock', 'chime', 'pay-phone', 'safety_pin', 'binoculars', 'sunglasses', 'necklace', 'chain', 'sunglass', 'milk_can', 'academic_gown',
        'backpack', 'beer_bottle', 'bubble', 'gas_pump', 'barometer', 'wine_bottle', 'water_bottle', 'mortarboard', 'abacus', 'rule', 'pop_bottle', 'pop_bottle', 'watch', 'collar',
        'bow', 'beer_glass', 'laptop', 'shield', 'wallet', 'thimble', 'envelope', 'seat_belt', 'tray', 'vase', 'necklace', 'lipstick', 'shower_cap', 'paper_towel', 'diaper', 
        'lipstick', 'pill_bottle', 'paper_towel', 'rubber_eraser', 'beaker', 'pick', 'hand_blower', 'Petri_dish', 'letter_opener', 'hair_slide',
    ],

    'гаджеты': [
        'wall_clock', 'digital_clock', 'cellular_telephone', 'notebook', 'crossword_puzzle', 'sax', 'loudspeaker', 'iPod', 'cassette_player', 'radio', 'digital_watch', 'dial_telephone', 
        'cicada', 'organ', 'joystick', 'space_heater', 'remote_control', 'CD_player', 'projector', 'microwave', 'home_theater', 'espresso_maker', 
    ],

    'люди': [
        'bridegroom', 'snowboarder', 'pirate', 'tick', 'Pekinese', 'cuirass', 'walking_stick', 'indigo_bunting', 'admiral', 'bolo_tie', 'ocarina', 
        "yellow_lady's_slipper",'scuba_diver', 
    ],

    'прочее': [
        'pedestal', 'beach_wagon', 'totem_pole', 'analog_clock', 'crutch', 'turnstile', 'bikini', 'tripod', 'pinwheel', 'jacamar', 'bubble', 'shower_cap',
        'bolo_tie', 'paper_towel', 'handkerchief', 'plunger', 'stopwatch', 'cornet', 'bucket', 'shoji', 'ant', 'nail', 'rifle', 'whistle', 'jigsaw_puzzle', 'cash_machine', 'web_site',
        'torch', 'menu', 'sorrel', 'radiator', 'cock', 'teddy', 'tabby', 'comic_book', 'binder', 'sock', 'wallet', 'revolver', 'Windsor_tie', 'jaj', 'muzzle', 'ear',
        'clumber', 'jay', 'ladle', 'affenpinscher', 'perfume', 'chest', 'bassoon', 'violin', 'bloodhound', 'American_egret', 'wool', 'lighter',
        'ballpoint', 'Shih-Tzu', 'velvet', 'typewriter_keyboard', 'Angora', 'tiger_beetle', 'mask', 'jersey', 'magnetic_compass', 'toaster', 'quill', 'jala',
        'harvestman', 'panpipe', 'dung_beetle', 'acoustic_guitar', 'pencil_box', 'barbell', 'Polaroid_camera', 'teapot', 
        'lorikeet', 'goblet', 'drum', 'weevil', 'Walker_hound', 'monarch', 'drumstick', 'sunscreen', 'cocktail_shaker', 'ping-pong_ball', 'cleaver', 
        'whiskey_jug', 'marimba', 'eel', 'candle', 'horned_viper', 'nematode', 'piggy_bank', 'ground_beetle', 'toilet_seat', 'spoonbill', 'iron',   'medicine_chest',
        'oxygen_mask', 'spaghetti_squash', 'rubber_eraser', 'rubber_eraser', 'matchstick', 'long-horned_beetle', 'black_and_gold_garden_spider', 
        'black_grouse', 'agama', 'matchstick', 'oxygen_mask', 'breastplate', 'strainer', 'paintbrush', 'packet', 'chickadee', 'leafhopper', 'acorn_squash', 'saltshaker', 'Crock_Pot', 'leafhopper', 
        'medicine_chest', 'rubber_eraser', 'trilobite', 'stinkhorn', 'hartebeest', 'red-breasted_merganser', 'soup_bowl', 'plate_rack', 'hot_pot', 'plate', 
        'plastic_bag',
    ]

}



def get_first_appropriate(media: list[Media]):
    for medium in media:
        for ext in ('.png', '.jpg', '.jpeg', '.webp'):
            if medium.link.lower().endswith(ext):
                return medium

class RecognitionModule(AnalysisModule):
    def extract(self, query: Query[Survey]) -> dict:
        return {
            'images': [
                get_first_appropriate(survey.media).filename for survey in query
            ]
        }

    def process(self, data: dict) -> dict:
        # Загрузка предварительно обученной модели VGG16
        model = VGG16(weights='imagenet')

        # Объединение классов объектов в более обобщенные категории

        # Создание словаря для отслеживания количества обобщенных категорий
        category_counts = {category: 0 for category in general_categories.keys()}
        category_images = {category: [] for category in general_categories.keys()}
        categories = []

        image_list = []
        for filename in data['images']:
            try:
                img = image.load_img(filename, target_size=(224, 224))
            except:
                print(f'BAD FILE: {filename}')
                image_list.append((None, None))
                continue

            img = image.img_to_array(img)
            img = np.expand_dims(img, axis=0)
            img = preprocess_input(img)
            image_list.append((filename, img))

        # TODO не закидывать картинки по одной
        for img_file, x in image_list:
            if img_file is None:
                categories.append(None)
                continue

            preds = model.predict(x)
            decoded_preds = decode_predictions(preds, top=2)[0]

            for i, (_, label, prob) in enumerate(decoded_preds):
                for category, classes in general_categories.items():
                    if label in classes:
                        category_counts[category] += 1
                        category_images[category].append(img_file)
                        
                        if i == 0:
                            categories.append(category)

                        break

                else:
                    if i == 0:
                        categories.append('прочее')

        return {
            'total_images': len(data['images']),
            'processed_images': len([d for d in categories if d is not None]),
            'category_counts': category_counts,
            'category_images': category_images,

            'columns': {
                'recognized': categories
            }
        }

    def display(self, data: dict) -> None:
        print('Всего фотографий проанализировано:', data['total_images'])
        print('Обобщенных категорий выделено:')
        for category, count in data['category_counts'].items():
            print(f'{category}: {count}')

        for category, images in data['category_images'].items():
            if images:
                print('{category}:\n{images}\n'.format(
                    category=category,
                    images='\n'.join(f'  {image}' for image in images)
                ))

        print(data['columns']['recognized'])


def main():
    query = Survey.query.filter(
        Survey.address.has(name='Волгоград')
    ).limit(10)

    recog = RecognitionModule()
    recog.extract_process_display(query)


def main2():
    line = '\n'.join([ 
        '{name}: {labels}.'.format(
            name=name.capitalize(),
            labels = ', '.join(labels)
        )
        
        for name, labels in general_categories.items()
    ])

    with open('classes.txt', 'w', encoding='utf-8') as file:
        file.write(line)


if __name__ == '__main__':
    main()
        
