import os
import json
from datetime import datetime
from sqlalchemy.orm import joinedload
from sqlalchemy import func, desc
from flask import request, jsonify, \
    send_from_directory, abort

from . import app, db
from .models import User, TelegramUser, WebUser, Survey, Tag, Media, Comment, Address
from .helpers import get_upload_filename, serialize_user
from .tasks.geocode import get_address_for


# origin_list = ['https://www.pravonagorod.urbanbasis.com', 'https://pravonagorod.urbanbasis.com', 'https://www.righttothecity.urbanbasis.com', 'https://righttothecity.urbanbasis.com']
@app.after_request
def add_cors_headers(response):
    # origin = request.headers.get('Origin')

    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
    response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS'
    return response


@app.route('/api/submit', methods=['POST'])
def submit():
    new_survey = Survey(
        lat=float(request.form.get('lat')),
        lon=float(request.form.get('lon')),
        category=request.form.get('categoryName'),
        rating=int(request.form.get('rating')),
        comment=request.form.get('comment'),
        user_id=int(request.form.get('userId')),
        date=datetime.utcnow(),
    )

    new_survey.address = get_address_for(new_survey)

    db.session.add(new_survey)
    db.session.flush()

    for file in request.files.values():
        unique_filename = get_upload_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'] / unique_filename))

        media = Media(link=str(unique_filename), survey_id=new_survey.id)
        db.session.add(media)
        db.session.flush()

    tags = request.form.get('tags')

    if tags:
        tag_list = tags.split(',')
        for tag in tag_list:
            new_tag = Tag(text=tag.strip().strip('#'), survey_id=new_survey.id)
            db.session.add(new_tag)

    db.session.commit()

    return jsonify({'message': 'Data saved successfully.'})


@app.route('/api/add_comment', methods=['POST'])
def add_comment():
    data = json.loads(request.data)
    comment_text = data['comment']
    survey_id = data['surveyId']
    userId = data['userId']

    comment = Comment(text=comment_text, user_id=userId,
                      survey_id=survey_id, date=datetime.utcnow())
    db.session.add(comment)
    db.session.commit()

    return jsonify({
        'comment_id': comment.id,
        'comment_text': comment_text,
        'user_id': userId,
        'survey_id': survey_id,
        'date': comment.date.strftime('%Y-%m-%d')
    })


@app.route('/api/user/<int:user_id>', methods=['PUT'])
def update_user(user_id):
    data = request.json

    user = WebUser.query.get(user_id)
    if not user:
        return jsonify({'error': 'User not found'}), 404

    if 'username' in data:
        user.username = data['username']
    if 'name' in data:
        user.name = data['name']
    if 'email' in data:
        user.email = data['email']

    db.session.commit()

    return jsonify(serialize_user(user)), 200

@app.route('/api/map')
def show_map():
    offset = request.args.get('offset', type=int)
    limit = request.args.get('limit', type=int)
    user_id = request.args.get('userId', type=int)

    query = Survey.query.options(
        joinedload(Survey.tags),
        joinedload(Survey.media),
        joinedload(Survey.user)
    ).order_by(desc(Survey.date))

    if user_id:
        query = query.filter(Survey.user_id == user_id)

    if offset is not None and limit is not None:
        query = query.offset(offset).limit(limit)
    else:
        pass

    surveys = query.all()

    survey_data = []

    for survey in surveys:
        survey_data.append({
            'id': survey.id,
            'lat': survey.lat,
            'lng': survey.lon,
            'rating': survey.rating,
            'category': survey.category,
            'date': survey.date.date().strftime('%Y-%m-%d'),
            'comment': survey.comment,
            'tags': [tag.text for tag in survey.tags],
            'media': [medium.url for medium in survey.media],
            'user': {'username': survey.user.username, 'type': survey.user.user_type},
            'user_login': survey.user.username,
        })

    total = db.session.query(func.count(Survey.id)).scalar()

    response = {
        'total': total,
        'data': survey_data,
    }
    
    return jsonify(response)


@app.route('/api/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        data = json.loads(request.data)

        username = data['login']
        password = data['password']
        
        user = WebUser.query.filter_by(username=username).first()

        if user and user.password == password:
            response_data = {
                'id': user.id,
                'username': user.username,
                'name': user.name,
                'priority': user.priority,
                'email': user.email,
            }

            return response_data
            
        error_message = 'Неверный логин или пароль'
        return jsonify({'error': error_message}), 401

    return abort(405)


@app.route('/api/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        data = json.loads(request.data)

        username = data['login']
        password = data['password']
        name = data['name']
        email = data['email']

        if WebUser.query.filter(WebUser.email == email).first():
            return {
                'error': 'Указанный адрес электронной почты занят'
            }, 409

        if WebUser.query.filter(WebUser.username == username).first():
            return {
                'error': 'Указанный логин занят'
            }, 409

        new_user = WebUser(
            username=username,
            password=password,
            name=name,
            priority=0,
            email=email,
        )
        db.session.add(new_user)
        db.session.commit()

        return serialize_user(new_user)

    return abort(405)


@app.route('/api/users', methods=['GET'])
def admin_dashboard():
    users = User.query.all()

    serialized_users = [serialize_user(user) for user in users]

    return serialized_users


@app.route('/api/user/dashboard')
def user_dashboard():
    # if current_user.priority == 1:
    #     flash('You are not authorized to access this page.', 'error')
    #     return redirect(url_for('admin_dashboard'))

    return { 'ok': 1 }

@app.route('/api/survey')
def survey():
    return { 'ok': 1 }

@app.route('/api/uploads/<path:filename>')
def uploads(filename):
    return send_from_directory(
        directory=app.config['UPLOAD_FOLDER'],
        path=filename,
        as_attachment=True
    )


@app.route('/api/user/<int:user_id>', methods=['GET'])
def get_user(user_id):
    user = TelegramUser.query.filter_by(telegram_id=user_id).first()

    if user is None:
        return {
            'success': False,
            'message': 'User not found'
        }, 404

    return {
        'success': True,
        'user': {
            'id': user.id,
            'telegram_id': user.telegram_id,
            'username': user.telegram_username,
        }
    }


@app.route('/api/user', methods=['POST'])
def create_user():
    tg_id = request.json.get('id')
    username = request.json.get('username')

    user = TelegramUser.query.filter_by(telegram_id=tg_id).first()

    if user is None:
        user = TelegramUser(
            telegram_id=tg_id,
            telegram_username=username,
        )
        db.session.add(user)
        db.session.commit()

        return {'success': True, 'id': user.id}, 201

    return {
       'success': False,
       'id': user.id,
       'message': 'Already exists'
    }, 409


@app.route('/api/survey', methods=['POST'])
def create_survey():
    data = request.json

    tg_id = data['user_id']

    user = TelegramUser.query.filter_by(telegram_id=tg_id).first()

    if user is None:
        return {
            'success': False,
            'message': 'User not found. Create user first'
        }, 400  # Не 404, так как юзер не запрашивается напрямую

    # TODO тут бы валидацию запилить и автоматом ошибки в json ответе сообщать
    survey = Survey(
        lat=data['position']['lat'],
        lon=data['position']['lon'],
        category=data['category'],
        rating=data['rating'],
        comment=data['comment'],
        date=datetime.utcnow(),
        user_id=user.id,
    )

    survey.address = get_address_for(survey)

    db.session.add(survey)
    db.session.flush()

    tags = data.get('tags')

    if tags:
        for tag in tags.split():
            new_tag = Tag(text=tag.strip().strip('#'), survey_id=survey.id)
            db.session.add(new_tag)

    db.session.commit()

    return {'success': True, 'id': survey.id}, 201


@app.route('/api/survey/<int:survey_id>/media', methods=['POST'])
def create_survey_media(survey_id):
    media = []

    for file in request.files.values():
        filename = get_upload_filename(file.filename)
        file.save(app.config['UPLOAD_FOLDER'] / filename)

        app.logger.info(
            f'Saved {file} to {app.config["UPLOAD_FOLDER"] / filename}'
        )

        medium = Media(link=str(filename), survey_id=survey_id)
        media.append(medium)
        db.session.add(medium)

    db.session.commit()

    if not len(media):
        return {
            'success': False,
            'message': 'No files attached'
        }, 400

    return {
        'success': True,
        'ids': [medium.id for medium in media]
    }, 201


@app.route('/api/analytics/ratings', methods=['GET'])
def analytics_ratings():
    # !!! По возможности избавиться от костыля
    # Фильтруем записи:
    # - только после 2024-03-22
    # - только от web-пользователей
    # - прибавляем 5 к каждой оценке
    web_ratings_after_date = db.session.query(
        (Survey.rating + 5).label('adjusted_rating'),
        func.count(Survey.id).label('count')
    ).join(User, Survey.user_id == User.id).filter(
        Survey.date > datetime(2024, 3, 22), 
        User.user_type == 'web'
    ).group_by('adjusted_rating').all()

    other_ratings = db.session.query(
        Survey.rating,
        func.count(Survey.id).label('count')
    ).join(User, Survey.user_id == User.id).filter(
        db.or_(
            Survey.date <= datetime(2024, 3, 22),
        )
    ).group_by(Survey.rating).all()

    total_responses = db.session.query(func.count(Survey.id)).scalar()

    if total_responses == 0:
        return jsonify([])

    rating_dict = {}

    for adjusted_rating, count in web_ratings_after_date:
        if adjusted_rating in rating_dict:
            rating_dict[adjusted_rating] += count
        else:
            rating_dict[adjusted_rating] = count

    for rating, count in other_ratings:
        if rating in rating_dict:
            rating_dict[rating] += count
        else:
            rating_dict[rating] = count

    result = []
    for rating, count in rating_dict.items():
        percentage = (count / total_responses) * 100
        result.append({
            'rating': rating,
            'count': round(percentage, 2)
        })

    total_percentage = sum(item['count'] for item in result)
    if total_percentage != 100.0:
        difference = 100.0 - total_percentage
        result[-1]['count'] = round(result[-1]['count'] + difference, 2)

    result.sort(key=lambda x: x['rating'])

    return jsonify(result)

@app.route('/api/analytics/categories', methods=['GET'])
def analytics_categories():
    category_counts = db.session.query(
        Survey.category,
        func.count(Survey.id).label('count')
    ).group_by(Survey.category).all()

    total_responses = db.session.query(func.count(Survey.id)).scalar()

    if total_responses == 0:
        return jsonify([])

    result = []
    for category, count in category_counts:
        percentage = (count / total_responses) * 100
        result.append({
            'category': category,
            'count': round(percentage, 2) 
        })

    return jsonify(result)

@app.route('/api/analytics/tags', methods=['GET'])
def analytics_tags():
    tag_counts = db.session.query(
        Tag.text,
        func.count(Tag.id).label('count')
    ).group_by(Tag.text).order_by(desc('count')).all()

    if not tag_counts:
        return jsonify({'top_tags': [], 'other_tags': []})

    top_tags = [{'text': tag, 'count': count} for tag, count in tag_counts[:5]]
    other_tags = [{'text': tag, 'count': count} for tag, count in tag_counts[5:]]

    return jsonify({
        'top_tags': top_tags,
        'other_tags': other_tags
    })


@app.route('/api/analytics/addresses', methods=['GET'])
def analytics_addresses():
    address_counts = db.session.query(
        Address.name,
        func.count(Survey.id).label('count')
    ).join(Survey, Survey.id == Address.survey_id).group_by(Address.name).order_by(desc('count')).all()

    if not address_counts:
        return jsonify({'topAddresses': [], 'otherAddresses': []})
    top_addresses = [{'address': address, 'count': count} for address, count in address_counts[:3]]
    other_addresses = [{'address': address, 'count': count} for address, count in address_counts[3:]]

    return jsonify({
        'topAddresses': top_addresses,
        'otherAddresses': other_addresses
    })