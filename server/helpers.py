import os
import uuid
from pathlib import Path

from werkzeug.utils import secure_filename

from . import app


def get_upload_filename(filename):
    name, ext = os.path.splitext(filename)

    # Убедиться что имя файла будет валидным
    secure = secure_filename(name)

    # Сгенерить рандомное, если имя не валидно
    if not secure:
        secure = uuid.uuid4().hex

    # Полный путь
    filename: Path = app.config['UPLOAD_FOLDER'] / secure

    new_filename = filename
    number = 1

    # Перебираем номер, пока не найдём свободный
    while new_filename.with_suffix(ext).exists():
        new_filename = filename.with_name(f'{filename.name}_{number}')
        number += 1

    return (
        new_filename
        .with_suffix(ext)
        .relative_to(app.config['UPLOAD_FOLDER'])
    )


def serialize_user(user):
    return {
        'id': user.id,
        'username': user.username,
        'name': user.name,
        'priority': getattr(user, 'priority', 0),
        'email': getattr(user, 'email', ''),
        'surveysNumber': len(user.surveys),
    }
